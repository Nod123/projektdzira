package com.example.projectdzira.repository;


import com.example.projectdzira.model.AppUser;
import com.example.projectdzira.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByActiveAndCreator(boolean active, AppUser creator);
//
//    @Query("SELECT t FROM Task AS t WHERE t.isActive = :status AND t.appUser = :user")
//    List<Task> displayUsersTasksWithGivenStatus(@Param("status") final boolean status,
//                                                @Param("user") final AppUser user);
}

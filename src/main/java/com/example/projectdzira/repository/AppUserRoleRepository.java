package com.example.projectdzira.repository;

import com.example.projectdzira.model.AppUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRoleRepository extends JpaRepository<AppUserRole, Long> {
    boolean existsByName(String name);

    AppUserRole findByName(String role);
}

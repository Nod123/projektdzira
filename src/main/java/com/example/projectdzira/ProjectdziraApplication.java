package com.example.projectdzira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectdziraApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectdziraApplication.class, args);
    }

}

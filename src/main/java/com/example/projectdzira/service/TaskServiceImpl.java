package com.example.projectdzira.service;


import com.example.projectdzira.exception.TaskNotFoundException;
import com.example.projectdzira.exception.UserNotFoundException;
import com.example.projectdzira.model.AppUser;
import com.example.projectdzira.model.Task;
import com.example.projectdzira.repository.AppUserRepository;
import com.example.projectdzira.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private AppUserRepository appUserRepository;

    @Autowired
    public TaskServiceImpl(final TaskRepository taskRepository, final AppUserRepository appUserRepository) {
        this.taskRepository = taskRepository;
        this.appUserRepository = appUserRepository;
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @Override
    public Set<Task> getAllTasksByUser(final String email) {
        Optional<AppUser> userOptional = appUserRepository.findByEmail(email);

        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("Unable to find user with this email address.");
        }

        return userOptional.get().getTasks();
    }

    @Override
    public List<Task> getTasksWithGivenStatusByUser(final boolean status, final String email) {
        Optional<AppUser> userOptional = appUserRepository.findByEmail(email);

        if (!userOptional.isPresent()){
            throw new UserNotFoundException("Unable to find user with this email address.");
        }

        AppUser appUser = userOptional.get();
        return taskRepository.findAllByActiveAndCreator(status, appUser);
    }

    @Override
    public Task addTask(String title, String content) {
        Task task = new Task(title, content, true);

        taskRepository.save(task);

        return task;
    }

    @Transactional
    @Override
    public void changeTaskStatus(final Long taskId, final boolean status) {
        Optional<Task> taskOptional = taskRepository.findById(taskId);

        if (!taskOptional.isPresent()){
            throw new TaskNotFoundException("Unable to find task.");
        }

        Task task = taskOptional.get();

        task.setActive(status);

        taskRepository.save(task);
    }

    @Transactional
    @Override
    public void addTaskToUser(Task task, String email) {
        Optional<AppUser> userOptional = appUserRepository.findByEmail(email);

        if (!userOptional.isPresent()){
            throw new UserNotFoundException("Unable to find user with this email address.");
        }

        AppUser appUser = userOptional.get();

        appUser.getTasks().add(task);
        task.setCreator(appUser);

        taskRepository.save(task);
        appUserRepository.save(appUser);
    }

    @Transactional
    @Override
    public void deleteTask(final Long taskId) {
        taskRepository.deleteById(taskId);
    }
}

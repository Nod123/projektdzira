package com.example.projectdzira.service;


import com.example.projectdzira.model.AppUserRole;

import java.util.Set;

public interface AppUserRoleService {
    Set<AppUserRole> getDefaultUserRoles();
}

package com.example.projectdzira.service;



import com.example.projectdzira.model.Task;

import java.util.List;
import java.util.Set;

public interface TaskService {

    List<Task> getAllTasks();

    Set<Task> getAllTasksByUser(final String email);

    List<Task> getTasksWithGivenStatusByUser(final boolean status, final String email);

    Task addTask(String title, String content);

    void changeTaskStatus(final Long taskId, final boolean status);

    void addTaskToUser(Task task, String email);

    void deleteTask(final Long taskId);
}

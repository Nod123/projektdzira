package com.example.projectdzira.service;

import com.example.projectdzira.model.AppUserRole;
import com.example.projectdzira.repository.AppUserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AppUserRoleServiceImpl implements AppUserRoleService {

    private final AppUserRoleRepository appUserRoleRepository;

    @Autowired
    public AppUserRoleServiceImpl(final AppUserRoleRepository appUserRoleRepository) {
        this.appUserRoleRepository = appUserRoleRepository;
    }

    @Value("${user.default.roles}")
    private String[] defaultRoles;

    @Override
    public Set<AppUserRole> getDefaultUserRoles() {
        HashSet<AppUserRole> roles = new HashSet<>();
        for (String role : defaultRoles) {
            roles.add(appUserRoleRepository.findByName(role));
        }

        return roles;
    }
}

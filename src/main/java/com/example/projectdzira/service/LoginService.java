package com.example.projectdzira.service;


import com.example.projectdzira.exception.UserNotFoundException;
import com.example.projectdzira.model.AppUser;
import com.example.projectdzira.model.AppUserRole;
import com.example.projectdzira.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {

    private AppUserRepository appUserRepository;

    @Autowired
    public LoginService(final AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        Optional<AppUser> optionalAppUser = appUserRepository.findByEmail(email);
        if(optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();

            String[] rolesArray = appUser.getRoles()
                    .stream()
                    .map(AppUserRole::getName)
                    .toArray(String[]::new);

            return User.builder()
                    .username(appUser.getEmail())
                    .password(appUser.getPassword())
                    .roles(rolesArray)
                    .build();
        }

        throw new UserNotFoundException("Unable to find user with this email address.");
    }

}

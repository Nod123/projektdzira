package com.example.projectdzira.service;


import com.example.projectdzira.model.AppUser;

import java.util.List;

public interface AppUserService {

    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();

    void deleteUser(Long userId);
}

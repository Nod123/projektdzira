package com.example.projectdzira.controller;


import com.example.projectdzira.service.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class AppUserController {

    private AppUserServiceImpl userServiceImpl;

    @Autowired
    public AppUserController(final AppUserServiceImpl userServiceImplementation) {
        this.userServiceImpl = userServiceImplementation;
    }

    @GetMapping("/register")
    public String getRegistrationForm() {
        return "userRegister";
    }

    @PostMapping("/register")
    public String submitRegisterForm(@RequestParam(name = "username") String username,
                                     @RequestParam(name = "password") String password,
                                     @RequestParam(name = "password-confirm") String passwordConfirm) {
        userServiceImpl.registerUser(username, password, passwordConfirm);
        return "redirect:/login";
    }
}

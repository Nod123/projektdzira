package com.example.projectdzira.controller;


import com.example.projectdzira.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/task/")
public class AdminTaskController {
    private TaskService taskService;

    @Autowired
    public AdminTaskController(final TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/list")
    public String getCompleteTaskListOfAllUsers(Model model){
        model.addAttribute("taskList", taskService.getAllTasks());

        return "taskListView";
    }

    @GetMapping("/listPerUser")
    public String getAllTasksByCurrentUser(@RequestParam(name = "email") String email,
                                           Model model) {

        model.addAttribute("taskList", taskService.getAllTasksByUser(email));

        return "taskListView";
    }

    @GetMapping("/delete")
    public String deleteTask(@RequestParam(name = "taskId") Long taskId,
                             HttpServletRequest request) {

        String referer = request.getHeader("Referer");
        taskService.deleteTask(taskId);

        return "redirect:" + referer;
    }

    @GetMapping("/setToActive")
    public String setToActive(@RequestParam(name = "taskId") Long taskId,
                              HttpServletRequest request) {

        String referer = request.getHeader("Referer");
        taskService.changeTaskStatus(taskId, true);

        return "redirect:" + referer;
    }
}

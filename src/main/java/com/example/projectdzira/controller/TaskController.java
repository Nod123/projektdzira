package com.example.projectdzira.controller;

import com.example.projectdzira.model.Task;
import com.example.projectdzira.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/task")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(final TaskService taskService) {

        this.taskService = taskService;
    }

    @GetMapping("/list")
    public String getAllTasksByCurrentUser(Principal principal,
                                           Model model) {

        String currentUsersUsername = principal.getName();
        model.addAttribute("taskList", taskService.getAllTasksByUser(currentUsersUsername));

        return "taskListView";
    }

    @GetMapping("/active")
    public String getActiveTasks(Principal principal,
                                 Model model) {

        String currentUsersUsername = principal.getName();
        model.addAttribute("taskList", taskService.getTasksWithGivenStatusByUser(true, currentUsersUsername));

        return "taskListView";
    }

    @GetMapping("/inactive")
    public String getInactiveTasks(Principal principal,
                                   Model model) {

        String currentUsersUsername = principal.getName();
        model.addAttribute("taskList", taskService.getTasksWithGivenStatusByUser(false, currentUsersUsername));

        return "taskListView";
    }

    @GetMapping("/add")
    public String getAddTaskForm() {
        return "taskAddForm";
    }

    @PostMapping("/add")
    public String submitAddTaskForm(@RequestParam(name = "title") String title,
                                    @RequestParam(name = "content") String content,
                                    Principal principal) {

        Task task = taskService.addTask(title, content);
        taskService.addTaskToUser(task, principal.getName());

        return "redirect:/task/list";
    }

    @GetMapping("/setToInactive")
    public String setToInactive(@RequestParam(name = "taskId") Long taskId,
                                HttpServletRequest request) {

        String referer = request.getHeader("Referer");
        taskService.changeTaskStatus(taskId, false);

        return "redirect:" + referer;
    }
}

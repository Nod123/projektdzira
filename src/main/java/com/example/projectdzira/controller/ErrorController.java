package com.example.projectdzira.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/error")
public class ErrorController {

    @GetMapping(path = "/")
    public String handleError(HttpServletRequest request,
                            Model model) {

        model.addAttribute("message", request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
        model.addAttribute("statusCode", request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        model.addAttribute("exception", request.getAttribute(RequestDispatcher.ERROR_EXCEPTION));
        model.addAttribute("exceptionType", request.getAttribute(RequestDispatcher.ERROR_EXCEPTION_TYPE));

        return "error";
    }
}

package com.example.projectdzira.configuration;


import com.example.projectdzira.model.AppUser;
import com.example.projectdzira.model.AppUserRole;
import com.example.projectdzira.model.Task;
import com.example.projectdzira.repository.AppUserRepository;
import com.example.projectdzira.repository.AppUserRoleRepository;
import com.example.projectdzira.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private AppUserRoleRepository userRoleRepository;
    private AppUserRepository appUserRepository;
    private PasswordEncoder passwordEncoder;
    private TaskService taskService;

    @Autowired
    public DataInitializer(final AppUserRoleRepository userRoleRepository, final AppUserRepository appUserRepository, final PasswordEncoder passwordEncoder, final TaskService taskService) {
        this.userRoleRepository = userRoleRepository;
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.taskService = taskService;
    }

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {
        createRoleIfNotExists("ADMIN");
        createRoleIfNotExists("USER");

        createUserIfNotExists("admin", "admin", "ADMIN", "USER");
        createUserIfNotExists("user", "user", "USER");
        createUserIfNotExists("exampleUser1@example.com", "samplepassword1", "USER");
        createUserIfNotExists("exampleUser2@example.com", "samplepassword2", "USER");

        AddSampleTasksToUser("admin");
        AddSampleTasksToUser("user");
        AddSampleTasksToUser("exampleUser1@example.com");
        AddSampleTasksToUser("exampleUser2@example.com");
    }

    private void createUserIfNotExists(final String username, final String password, final String... roles) {
        if (!appUserRepository.existsByEmail(username)) {
            AppUser appUser = new AppUser();
            appUser.setEmail(username);
            appUser.setPassword(passwordEncoder.encode(password));

            appUser.setRoles(new HashSet<>(findRoles(roles)));

            appUserRepository.save(appUser);
        }
    }

    private List<AppUserRole> findRoles(final String[] roles) {
        List<AppUserRole> userRoles = new ArrayList<>();
        for (String role : roles) {
            userRoles.add(userRoleRepository.findByName(role));
        }
        return userRoles;
    }

    private void createRoleIfNotExists(final String roleName) {
        if (!userRoleRepository.existsByName(roleName)) {
            AppUserRole role = new AppUserRole();
            role.setName(roleName);

            userRoleRepository.save(role);
        }
    }

    @Transactional
    void AddSampleTasksToUser(final String username) {
//    void AddSampleTasksToUserIfTaskListEmpty(final String username) {
//        if (taskListIsEmpty(username)) {
        createTaskAndAssignToUser(username, "Very important task", "Do not forget to complete me!");
        createTaskAndAssignToUser(username, "Less important task", "Try to do this one today, but don't stress about it.");
        createTaskAndAssignToUser(username, "Not important task", "Do at some point in life.");
//        }
    }

    //TODO: figure out why throws lazy initialization
//    @Transactional
//    boolean taskListIsEmpty(final String username) {
//        boolean isEmpty = false;
//
//        Optional<AppUser> userOptional = appUserRepository.findByEmail(username);
//
//        if (!userOptional.isPresent()) {
//            throw new UserNotFoundException("Unable to find user with this email address.");
//        }
//
//        AppUser user = userOptional.get();
//        int tasksNumber = user.getTasks().size();
//
//        if (tasksNumber == 0) {
//            isEmpty = true;
//        }
//
//        return isEmpty;
//    }

    void createTaskAndAssignToUser(final String username, final String title, final String content) {
        Task task = taskService.addTask(title, content);
        taskService.addTaskToUser(task, username);
    }
}

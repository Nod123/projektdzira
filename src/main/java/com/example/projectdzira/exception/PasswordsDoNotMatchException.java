package com.example.projectdzira.exception;

public class PasswordsDoNotMatchException extends RuntimeException {
    public PasswordsDoNotMatchException(final String message) {
        super(message);
    }
}

package com.example.projectdzira.exception;

public class PasswordNotLongEnoughException extends RuntimeException {
    public PasswordNotLongEnoughException(final String message) {
        super(message);
    }
}

package com.example.projectdzira.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String email;

    private String password;

    @ManyToMany
    private Set<AppUserRole> roles;

    @OneToMany(mappedBy = "assignee")
    private Set<Task> tasks;

    @OneToMany(mappedBy = "user")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Activity> activities;
}

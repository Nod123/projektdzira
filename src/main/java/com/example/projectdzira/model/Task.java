package com.example.projectdzira.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String title;

    private String content;

    @CreationTimestamp
    private LocalDateTime created;

    private LocalDateTime finished;

    private boolean active;

    @ManyToOne()
    @EqualsAndHashCode.Exclude
    private Project project;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    public AppUser assignee;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private AppUser creator;

    @OneToMany(mappedBy = "task")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Activity> activities;

    public Task(final String title, final String content, final boolean active) {
        this.title = title;
        this.content = content;
        this.active = active;
    }
}

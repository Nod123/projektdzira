package com.example.projectdzira.model;

public enum TaskStatus {
    BACKLOG,
    SELECTED_FOR_DEVELOPMENT,
    IN_PROGRESS,
    DONE,
    VERIFIED
}
